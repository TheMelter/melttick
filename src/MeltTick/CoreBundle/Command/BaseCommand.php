<?php
namespace MeltTick\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

abstract class BaseCommand extends ContainerAwareCommand
{
    protected $commandName;
    protected $commandDescription;
    protected $dialog;
    protected $output;
    protected $input;
    protected $rootDir;
    protected $uploadDir;
    protected $baseUrl;
    protected $baseUrlProtocol;

    /** 
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->init();
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription);

        $this->baseUrlProtocol = "https://";
        $this->baseUrl = ".tickspot.com/api/";
        // "clients?email=me@company.com&password=mypass";
    }

    /**
     * Adds user related arguments.
     */
    protected function addUserArguments()
    {
        $this->addArgument(
            'company',
            InputArgument::REQUIRED,
            'Name of the company used in the subdomain of tick.'
        );
        $this->addArgument(
            'email',
            InputArgument::REQUIRED,
            'Email'
        );
        $this->addArgument(
            'password',
            InputArgument::REQUIRED,
            'Password'
        );
    }

    /**
     * Initializes member variables
     * Need to override commandName & commandDescription in derived classes.
     */
    protected function init()
    {
    }

    /**
     * Initializes services once executed
     */
    protected function initializeServices()
    {
        $this->dialog = $this->getHelperSet()->get('dialog');
        $this->rootDir = realpath($this->get('kernel')->getRootDir() . "/../");
        $this->uploadDir = $this->rootDir . '/web/uploads/';
    }

    /**
     * Shortcut for getting container services
     */
    protected function get($service)
    {
        return $this->getContainer()->get($service);
    }

    /**
     * Executes command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->input = $input;
        $this->initializeServices();
        $this->postServiceInit();
        $this->executeCommand();
    }

    /**
     * Function executed right after services are initialized.
     */
    protected function postServiceInit() {}

    /**
     * Executes command. Created so that it can be overriden without parameters.
     */
    protected function executeCommand() {}

    /**
     * Regular curls. Used for redirected urls.
     */
    protected function curl($url)
    {
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        $output = curl_exec($ch); 
        $info = curl_getinfo($ch);

        curl_close($ch);

        if ($info['http_code'] == 200) {
            return $output;
        }

        return null;
    }

    /**
     * Returns user parameters for url.
     */
    protected function getUserParameters($email, $password)
    {
        return "email=" . urlencode($email) . "&password=" . urlencode($password);
    }

    /**
     * Returns url.
     */
    protected function getApiUrl($method)
    {
        $company = $this->input->getArgument('company');
        $email = $this->input->getArgument('email');
        $password = $this->input->getArgument('password');
        $parameters = $this->getUserParameters($email, $password);

        return $this->baseUrlProtocol . $company . $this->baseUrl . $method . '?' . $parameters;
    }

}
