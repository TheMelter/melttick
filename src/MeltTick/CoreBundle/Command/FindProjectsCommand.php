<?php
namespace MeltTick\CoreBundle\Command;

class FindProjectsCommand extends BaseCommand
{
    /**
     * {@inheritDoc}
     */
    protected function init()
    {
        $this->commandName = 'mt:find:projects';
        $this->commandDescription = 'Lists projects for account';
        $this->addUserArguments();
    }

    /**
     * {@inheritDoc}
     */
    protected function executeCommand()
    {
        $url = $this->getApiUrl('projects');
        $response = $this->curl($url);

        $projects = new \SimpleXMLElement($response);

        foreach ($projects as $project) {
            $tasks = $project->tasks->task;
            echo "\nShowing tasks for Project: " . $project->name . " (id: " . $project->id . ")\n";

            $counter = 0;
            foreach ($tasks as $task) {
                $counter++;
                echo "$counter) " . $task->name . " (id: " . $task->id . ")\n";
            }
        }
    }
}

