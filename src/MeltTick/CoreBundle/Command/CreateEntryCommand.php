<?php
namespace MeltTick\CoreBundle\Command;

use Symfony\Component\Console\Input\InputArgument;

class CreateEntryCommand extends BaseCommand
{
    const PARAMETER_TASK_ID = 'task_id';
    const PARAMETER_DATE = 'date';
    const PARAMETER_HOURS = 'hours';
    const PARAMETER_START_DATE = 'start_date';
    const PARAMETER_END_DATE = 'end_date';

    /**
     * {@inheritDoc}
     */
    protected function init()
    {
        $this->commandName = 'mt:create:entry';
        $this->commandDescription = 'Creates entry for account.';
        $this->addUserArguments();
        $this->addArgument(
            self::PARAMETER_TASK_ID,
            InputArgument::REQUIRED,
            self::PARAMETER_TASK_ID
        );
        $this->addArgument(
            self::PARAMETER_HOURS,
            InputArgument::REQUIRED,
            self::PARAMETER_HOURS
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function executeCommand()
    {
        $today = new \DateTime();

        // Check if weekend.
        if ($today->format('D') == "Sat" || $today->format('D') == "Sun") {
            echo "Today is a weekend!\n";
            return;
        }

        // Get existing entries for today.
        $url = $this->getApiUrl('entries');
        $this->addStartDateToUrl($url, $today);
        $this->addEndDateToUrl($url, $today);
        $response = $this->curl($url);

        $entries = new \SimpleXMLElement($response);

        // Check if entries already entered.
        if (count($entries) === 0) {
            $url = $this->getApiUrl('create_entry');
            $this->addTaskIdToUrl($url);
            $this->addDateToUrl($url, $today);
            $this->addHoursToUrl($url);

            $response = $this->curl($url);
            echo "Successfully entered hours for: " . $today->format('Y-m-d') . "\n";
        } else {
            echo "You have already entered hours for the day!\n";
        }
    }

    /**
     * Add task id to url parameter.
     */
    private function addTaskIdToUrl(&$url)
    {
        $taskId = $this->input->getArgument(self::PARAMETER_TASK_ID);
        $url .= "&" . self::PARAMETER_TASK_ID . "=$taskId";
    }

    /**
     * Add date to url parameter.
     */
    private function addDateToUrl(&$url, $date)
    {
        $url .= "&" . self::PARAMETER_DATE . "=" . $date->format('Y-m-d');
    }

    /**
     * Add start date to url parameter.
     */
    private function addStartDateToUrl(&$url, $date)
    {
        $url .= "&" . self::PARAMETER_START_DATE . "=" . $date->format('Y-m-d');
    }

    /**
     * Add end date to url parameter.
     */
    private function addEndDateToUrl(&$url, $date)
    {
        $url .= "&" . self::PARAMETER_END_DATE . "=" . $date->format('Y-m-d');
    }

    /**
     * Add hours to url parameter.
     */
    private function addHoursToUrl(&$url)
    {
        $hours = $this->input->getArgument(self::PARAMETER_HOURS);
        $url .= "&" . self::PARAMETER_HOURS . "=" . $hours;
    }

}
