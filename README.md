Find task ids using:

    php app/console mt:find:projects bcgtrans email password


Enter hours using:

    php app/console mt:create:entry bcgtrans email password task_id hours

Cron example (5pm Mon-Fri logs 8 hours):

    00 17 * * 1-5 php /var/www/MeltTick/app/console mt:create:entry bcgtrans youremail@example.com yourpassword task_id 8


